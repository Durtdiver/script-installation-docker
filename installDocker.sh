#!/bin/bash

command_exists() {
	command -v "$@" > /dev/null 2>&1
}

if command_exists apt-get update; then
	if command_exists lsb_release; then
                echo lsb_release commande is already installed
        else
                #install lsb-release and lsb-core package
                apt-get install -y lsb-release
		apt-get install -y lsb-core
        fi
elif command_exists yum update; then
	if command_exists lsb_release; then
		echo lsb_release commande is already installed
	else
		#install redhat-lsb package
		yum install -y redhat-lsb
	fi
else
	echo The release is not supported by this script
	exit 1
fi

#check if lsb_release commande exist
if command_exists lsb_release; then
	#detect distribution and his version
	lsb_dist="$(lsb_release -si)"
	lsb_release="$(lsb_release -rs | cut -f 1 -d '.')"

	#test of the distribution and of it version
	if [ "$lsb_dist" == "CentOS" ]; then
		if [ "$lsb_release" == "7" ]; then
			#update pakage
			yum update

			#add the yum repo
			echo [dockerrepo]>>/etc/yum.repos.d/docker.repo
			echo name=Docker Repository>>/etc/yum.repos.d/docker.repo
			echo baseurl=https://yum.dockerproject.org/repo/main/centos/7/>>/etc/yum.repos.d/docker.repo
			echo enabled=1>>/etc/yum.repos.d/docker.repo
			echo gpgcheck=1>>/etc/yum.repos.d/docker.repo
			echo gpgkey=https://yum.dockerproject.org/gpg>>/etc/yum.repos.d/docker.repo

			#install the Docker
			yum install -y docker-engine

			#start Docker daemon
			service docker start

			#start Docker daemon at boot
			chkconfig docker on
			
			#install epel-release package that includes gpg keys for package signing and repository information.
			yum install -y epel-release
			
			#install pip
			yum install -y python-pip
			
			#install docker-compose
			pip install docker-compose
		fi
	elif [ "$lsb_dist" == "Debian" ]; then
		if [ "$lsb_release" == "8" ];then
			#update apt repository
			apt-get purge lxc-docker*
 			apt-get purge docker.io*
			apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
			echo deb https://apt.dockerproject.org/repo debian-jessie main>>/etc/apt/sources.list.d/docker.list
			apt-get install -y apt-transport-https
			apt-get update
			apt-cache policy docker-engine
			
			#install Docker
			apt-get update
			apt-get install -y docker-engine
			
			#start Docker daemon
			service docker start
			
			#install pip
			apt-get install -y python-pip
			
			#install docker-compose
			pip install docker-compose
		fi
	elif [ "$lsb_dist" == "Ubuntu" ]; then
                if [ "$lsb_release" == "14" ];then
                        #update apt repository
			apt-get update
                        apt-get install apt-transport-https ca-certificates
			apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
			echo deb https://apt.dockerproject.org/repo ubuntu-trusty main>>/etc/apt/sources.list.d/docker.list
			apt-get update
			apt-get purge lxc-docker
			#apt-get install -y linux-image-extra-$(uname -r)
			apt-get install -y apparmor

                        #install Docker
                        apt-get update
                        apt-get install -y docker-engine

                        #start Docker daemon
                        service docker start
			
			#start docker service on boot
			systemctl enable docker

                        #install pip
                        apt-get install -y python-pip

                        #install docker-compose
                        pip install docker-compose
                elif [ "$lsb_release" == "16" ];then
			#update apt repository
                        apt-get update
                        apt-get install apt-transport-https ca-certificates
                        apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
                        echo deb https://apt.dockerproject.org/repo ubuntu-xenial main>>/etc/apt/sources.list.d/docker.list
                        apt-get update
                        apt-get purge lxc-docker
                        #apt-get install -y linux-image-extra-$(uname -r)
                        apt-get install -y apparmor

                        #install Docker
                        apt-get update
                        apt-get install -y docker-engine

                        #start Docker daemon
                        service docker start

                        #start docker service on boot
                        systemctl enable docker

                        #install pip
                        apt-get install -y python-pip

                        #install docker-compose
                        pip install docker-compose
		fi
        fi
else
	echo lsb_release commande is not installed on this machin
fi
